package pl.mateuszpartyka.oor1;

import java.util.concurrent.Semaphore;

public class Main {
    Semaphore semaphore = new Semaphore(1);

    public static void main(String[] args) {
        Main app = new Main();
        Thread thread1, thread2, thread3;

        thread1 = new Thread() {
            @Override
            public void run() {
                app.testSemaphore();
            }
        };

        thread2 = new Thread() {
            @Override
            public void run() {
                app.testSemaphore();
            }
        };

        thread3 = new Thread() {
            @Override
            public void run() {
                try {
                    System.out.println("Oczekiwanie na drugi...");
                    // poczekaj aż skończy się wątek numer 2
                    thread2.join();
                    System.out.println("Drugi skończony.");

                    for (int i = 5; i > 0; i--) {
                        System.out.println(i + "...");
                        Thread.sleep(1000);
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {

                }
            }
        };

        // nadanie wątkom nazw aby je odróżnić
        thread1.setName("Pierwszy");
        thread2.setName("Drugi");

        // uruchomienie wszystkich wątków
        thread1.start();
        thread2.start();
        thread3.start();
    }

    private void testSemaphore() {
        try {
            // zablokowanie
            semaphore.acquire();

            for (int i = 0; i < 20; i++) {
                System.out.println(Thread.currentThread().getName());
                Thread.sleep(150);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            // zwolnienie
            // w finally, aby zwolnić nawet mimo błędów
            semaphore.release();
        }
    }
}
